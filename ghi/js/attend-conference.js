window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIconWrapper = document.getElementById('loading-conference-spinner');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

    // Here, add the 'd-none' class to the loading icon
    loadingIconWrapper.classList.add('d-none');
    // Here, remove the 'd-none' class from the select tag
    // const dNone = document.querySelector('.d-none') -- don't need
    selectTag.classList.remove('d-none')

    }
  
  });


const attendeeFormTag = document.getElementById('create-attendee-form')
attendeeFormTag.addEventListener('submit', async (event) => {
    event.preventDefault()

const attendeeFormData = new FormData(attendeeFormTag); 
const json = JSON.stringify(Object.fromEntries(attendeeFormData));


const attendeeUrl = 'http://localhost:8001/api/attendees/';
const fetchConfig = {
  method: "post",
  body: json,
  headers: {
    'Content-Type': 'application/json',
  },
};
const response = await fetch(attendeeUrl, fetchConfig);
if (response.ok) {
   const alertNoneRemove = document.getElementById('success-message')
   alertNoneRemove.classList.remove('d-none')
  attendeeFormTag.classList.add('d-none');
  const newAttendee = await response.json();
  console.log(newAttendee);
}


})
