function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class="col-sm mb-4">
      <div class="card shadow-lg mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          </div>
          <div class="card-footer"> ${conferenceDate(start)} - ${conferenceDate(end)}</div>

      </div>
        </div>
      </div>
    `;
  }

  function conferenceDate(time){
    const date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth()+1
    const day = date.getDate()
    return `${month}/${day}/${year}`
  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        const column = document.querySelector('.error')
        const html =  `<div class ="alert alert-danger"> Error code: ${response.status}. Details: ${response.statusText}  </div>`
        column.innerHTML += html;

    } else {
        const data = await response.json();
        
        for (let conference of data.conferences) {
            console.log(conference.id)
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const start = details.conference.starts
                const end = details.conference.ends
                const location = details.conference.location['name']
                const html = createCard(name, description, pictureUrl, start, end, location);
                const column = document.querySelector('.col');
                column.innerHTML += html;
          }
      }
    }
    } catch (e) {
        const column = document.querySelector('.error')
        const html =  `<div class ="alert alert-danger"> Error: ${e}. Details: ${response.statusText}  </div>`
        column.innerHTML += html;
    }
  });



  /////-------------------------CLASS WALKTHRU ----------------------//

//   // populate  category select
//   // grab select element
//   const select_el = document.querySelector('#category');
//   const category_url = "hhtps://jservice.xyz/api/categories";
//   // fetch for the category options
//   const categories_res = await fetch(category_url)
//   let category_data = await categories_res.json();
//   let top100 = category_data.categories.slice(0, 100)
//   console.log(categories_res)
//   // loop over options
//   top100.forEach((obj) =>{
//     let option_id = obj.id;
//     let value = obj.title;
//     let option_el = `
//     <option value='${option_id}'> ${value}</option>
//     `
//     // add that option to the select
//     select_el.innerHHTML += option_el

// // ANOTHER way to do this
//     // let option_el = document.createElement('option')
//     // select_el.append(option_el)
//   })
//     // craft an option from the category

// // SUBMIT FORM



// const form = document.querySelector('form')
// // how to override default submit
// form.addEventListener('submit', async event => { 
//   event.preventDefault();
//   console.log('stuff was prevented'); // this will prevent anything from happening


//   // grab the form data
//   const form_data = new FormData(form);
//   const data_obj = Object.fromEntries(formData.entries());
//   console.log(form_obj)

// // CREATE POST REQUEST

// const fetch_options = {
//   method: "post",
//   body: JSON.stringify(form_data_obj)
//   headers: {
//     "Content-Type": 'application/json'
//   }
// }
//   const post_url = "https://jservice.qyx/api/clues";
//   const post_res = await fetch(post_url, fetch_options);
//   console.log(post_res)

//   if (post_res.ok){
//     let post_json = await post_res.json() // we didn't we have to use await in python
//     console.log()
//     let category = post_json.category.title;
//     let question = post_json.question;
//     let answer
//   }

// })

