

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    const res = await fetch(url)

    if (res.ok){
        const data = await res.json()
        console.log(data)

        const selectTag = document.getElementById('location')

        for (let location of data.locations){
            const html = `<option value='${location.id}'>${location.name}</option>`
            selectTag.innerHTML += html
        }
      

    }
    // create JS file to handle loading the available locations 
const conferenceTag = document.getElementById('create-conference-form')
conferenceTag.addEventListener('submit', async event => {
    event.preventDefault()

    const conferenceFormData = new FormData(conferenceTag); 
    const json = JSON.stringify(Object.fromEntries(conferenceFormData));
    console.log(json)
    const conferenceURL = "http://localhost:8000/api/conferences/"
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json'
        }
    }
    const response = await fetch(conferenceURL, fetchConfig)
    if (response.ok) {
    conferenceTag.reset()
      const newConference = await response.json()
      console.log(newConference)
    }
  
})

})




