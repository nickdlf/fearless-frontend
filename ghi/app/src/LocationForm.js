import React , { useEffect, useState } from 'react';

function LocationForm() {
    const [name, setName] = useState('')
    const [states, setStates] = useState([]);
    const [state, setState] = useState('')
    const [city, setCity] = useState('')
    const [roomCount, setRoomCount] = useState('')

    const fetchData = async () => {
      const url = 'http://localhost:8000/api/states/';
  
      const response = await fetch(url);
  
      if (response.ok) {
        const data = await response.json();
        setStates(data.states)
      }
    }
    const handleStateChange= (event) =>{setState(event.target.value)}
    const handleNameChange = (event)=>{
        const value = event.target.value;
        setName(value)
    }
    
    const handleCityChange = (event) =>{
        // console.dir(event)
        const value = event.target.value;
        setCity(value)
    }

    const handleRoomCountChange = (event) =>{
        // console.dir(event)
        const value = event.target.value;
        setRoomCount(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {};
        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = state;
        console.log(data);
      
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);
          setName('');
          setRoomCount('');
          setCity('');
          setState('');
        }
      }
  
  
    useEffect(() => {
      fetchData();
    }, []);
      

        // const selectTag = document.getElementById('state');
        // for (let state of data.states) {
        //   const option = document.createElement('option');
        //   option.value = state.abbreviation;
        //   option.innerHTML = state.name;
        //   selectTag.appendChild(option);

  

        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create a new location</h1>
                  <form id="create-location-form" onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                      <input value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" onChange={handleNameChange} />
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={roomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" onChange={handleRoomCountChange} />
                      <label htmlFor="room_count">Room count</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={city} placeholder="City" required type="text" name="city" id="city" className="form-control" onChange={handleCityChange} />
                      <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                      <select value={state} required name="state" id="state" className="form-select" onChange={handleStateChange}>
                        <option defaultValue value="">Choose a state</option>
                        {states.map(state => <option key={state.abbreviation} value={state.abbreviation}>{state.name}</option>)}
                      </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
          );
        }
        
export default LocationForm;