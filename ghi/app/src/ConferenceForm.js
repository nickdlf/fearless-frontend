import React , { useEffect, useState } from 'react';

function ConferenceForm() {
    const [name, setName] = useState('')
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('')
    const [description, setDescription] = useState('')
    const [max_presentations, setMaxPresentations] = useState('')
    const [max_attendees, setMaxAttendees] = useState('')
    const [location, setLocation] = useState('')
    const [locations, setLocations] = useState([])

    const handleNameChange = (event) => { setName(event.target.value) }
    const handleStartsChange = (event) => { setStarts(event.target.value) }
    const handleEndsChange = (event) => { setEnds(event.target.value) }
    const handleDescriptionChange = (event) => { setDescription(event.target.value) }
    const handleMaxPresentationsChange = (event) => { setMaxPresentations(event.target.value) }
    const handleMaxAttendeesChange = (event) => { setMaxAttendees(event.target.value) }
    const handleLocationChange = (event) => { setLocation(event.target.value) } 

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
      
        const response = await fetch(url);
      
        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
      }

    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {}
          data.name = name;
          data.starts =starts ;
          data.ends = ends;
          data.description = description;
          data.max_presentations = max_presentations; 
          data.max_attendees = max_attendees;
          data.location = location;

      
        console.log(data);
      
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);
          setName('');
          setStarts('');
          setEnds('');
          setDescription('');
          setMaxPresentations('');
          setMaxAttendees('');
        //   setLocations("")
          setLocation('');
        }
      }
    
    useEffect(() => {
      fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form" onSubmit={handleSubmit}>
              {/* <!-- Name --> */}
              <div className="mb-3">
                <input value={name} htmlFor="name" placeholder="Name" required type="text" name="name" id="name" className="form-control" onChange={handleNameChange} />
              </div>
              {/* <!-- Start Date --> */}
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control" />
                <label htmlFor="starts">Start Date</label>
              </div>
              {/* <!-- End Date --> */}
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">End Date</label>
              </div>
              {/* <!-- Description --> */}
              <div className="form-floating mb-3">
                <textarea onChange={handleDescriptionChange} value={description} className="form-control" placeholder="Enter a description" name="description" id="description" rows="5" />
                <label htmlFor="description">Description</label>
              </div>

            {/* <!-- Maximum Presentations --> */}
            <div className="mb-3">
                <input onChange={handleMaxPresentationsChange} value={max_presentations} placeholder="Maximum Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
            {/* <!-- Maximum Attendees --> */}
            <div className="mb-3">
                <input onChange={handleMaxAttendeesChange} value={max_attendees} placeholder="Maximum Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>

            {/* <!-- Choose Location --> */}
            <div className="mb-3">
            <select value={location} required name="location" id="location" className="form-select" onChange={handleLocationChange}>
                        <option defaultValue value="">Choose a location</option>
                        {locations.map(location => <option key={location.id} value={location.id}>{location.name}</option>)}
                      </select>
                {/* <select required name="locations" id="locations" className="form-select" onChange={handleLocationChange} value={locations}> 
                <option defaultValue value="">Choose a Conference Location</option>
                  {locations.map(location => <option key={location.id} value={location.id}>{location.name}</option>)} 
                </select> */}
            </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
    }
    
export default ConferenceForm;
