import React, { useEffect, useState } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState('');
    const [presenter_name, setPresenter] = useState('');
    const [company_name, setCompany] = useState('');
    const [presenter_email, setEmail] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        
        const data = {};
        data.presenter_name = presenter_name;
        data.conference = conference;
        data.presenter_name = presenter_name;
        data.company_name = company_name;
        data.presenter_name = presenter_email;
        data.title = title;
        data.synopsis = synopsis;
        console.log(`CONFERENCE = ${conference}`) // this WILL = the ID but not sure how?
        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);

        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            setConference('');
            setPresenter('');
            setCompany('');
            setEmail('');
            setTitle('');
            setSynopsis('');
        };
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form id="create-presentation-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input value={presenter_name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" onChange={(e) => setPresenter(e.target.value)} />
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={presenter_email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" onChange={(e) => setEmail(e.target.value)} />
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={company_name} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" onChange={(e) => setCompany(e.target.value)} />
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control" onChange={(e) => setTitle(e.target.value)} />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis" onChange={(e) => setSynopsis(e.target.value)}></textarea>
                        </div>
                        <div className="mb-3">
                            <select value={conference} required name="conference" id="conference" className="form-select" onChange={(e) => setConference(e.target.value)}>
                                <option defaultValue value="">Choose a conference</option>
                                {conferences.map((conference) => <option key={conference.id} value={conference.id}>{conference.name}</option>)}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default PresentationForm;