import React, {useEffect, useState} from 'react';

function AttendConferenceForm () {
    const [name, setName] = useState('');
    const[email, setEmail] = useState('');
    const[conference, setConference] =useState('');
    const[conferences, setConferences] = useState([]);
    const [successClasses, setSuccessClasses] = useState("alert alert-success d-none mb-0");
    let spinnerClasses = "d-flex justify-content-center mb-3"
    let dropdownClasses = "form-select d-none"

    // const handleSuccessChange = (event) => { setSuccessClasses(event.target.value) }
    const handleNameChange = (event) => { setName(event.target.value) }
    const handleEmailChange = (event) => { setEmail(event.target.value)}
    const handleConferenceChange = (event) => { setConference(event.target.value)}


    if (conferences.length>0) {
      spinnerClasses = "d-none"
      dropdownClasses = "form-select"
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name
        data.email = email
        data.conference = conference



        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(attendeeUrl, fetchConfig);

        if (response.ok) {

            const attendee = await response.json();
            console.log("attendee = ", attendee);

            setName('');
            setEmail("");
            setConference("");
            setSuccessClasses("alert alert-success mb-0")


        }

      }




    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences)
          }
        }

        // using useEffect like this prevents errors
      useEffect(() => { // we useEffect because this syntax causes it to only run on initial render
        fetchData();
      }, []);

    return (
<div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" alt="Logo Description" src="/logo.svg"/>
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className={spinnerClasses} id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleConferenceChange} name="conference" id="conference" value={conference} className={dropdownClasses} required>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                        <option key={conference.id} value={conference.href}>
                            {conference.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input required onChange={handleNameChange} value={name} placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input required onChange={handleEmailChange} value={email} placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className={successClasses} id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
}

export default AttendConferenceForm;
